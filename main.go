package main

import (
	"fmt"
	"math"
	"os"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
)

func main() {
	var duration, minute, second, numOfIntervals int
	var intervals []int
	fmt.Print("Введите количество минут: ")
	fmt.Scan(&minute)
	fmt.Print("Введите количество секунд: ")
	fmt.Scan(&second)
	fmt.Print("Введите количество интервалов: ")
	fmt.Scan(&numOfIntervals)
	for i:= 0; i < numOfIntervals; i++ {
		fmt.Printf("Введите длину %v интервала\n", i + 1)
		var input int;
		fmt.Scan(&input)
		intervals = append(intervals, input)
	}

	duration = ((minute * 60) + (second)) * int(time.Second)

	startTime := time.Now() 
	ticker := time.NewTicker(1 * time.Second)

	done := make(chan bool)

	go func () {
		defer ticker.Stop()
		intervalIterator := 0
		numOfIntervalIterator := 0
		for  {
			<-ticker.C
			elapsedTime := time.Since(startTime)
			remainingTime := (duration - int(elapsedTime) + 100000000) / 1000000000
			if intervals[numOfIntervalIterator] == intervalIterator {
				playSound("stop.mp3")
				intervalIterator = 0
				numOfIntervalIterator++
				if numOfIntervalIterator >= len(intervals) {
					numOfIntervalIterator = 0
				}
			}
			if remainingTime <= 0 {
				fmt.Println("Завершено")
				done <- true
				return
			}
			intervalIterator++
			remainingTime = int(math.Round(float64(remainingTime)))
			fmt.Printf("[+] %v:%v\n", remainingTime / 60, remainingTime % 60)
		}
	}()

	<-done
	playSound("stop.mp3")
	playSound("stop.mp3")
	playSound("stop.mp3")
}

func playSound(soundPath string) {
	file, err := os.Open(soundPath)
	if err != nil {
		panic(err)
	}

	defer file.Close()

	streamer, format, err := mp3.Decode(file)
	if err != nil {
		panic(err)
	}

	defer streamer.Close()

	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))
	done := make(chan struct{})
	speaker.Play(beep.Seq(streamer, beep.Callback(func ()  {
		close(done)
	})))
	<-done
}
